    <section class="z-index-1 pull-bottom-fixed full-width" data-pages="reveal-footer">
      <section class="p-t-10 p-b-10 bg-master-darkest">
        <div class="container">
          <div class="row">
            <div class="col-sm-7">
              <p class="fs-11 no-margin font-arial text-white small-text"><span class="hint-text">If you want to go quickly,</span> go alone. <span class="hint-text">If you want to go far,</span> go together.
              </p>
            </div>
            <div class="col-sm-5 text-right">
              <p class="fs-11 no-margin font-arial text-white small-text">Copyright © <?php echo date('Y');?> Vogsphere. All Rights Reserved.
              </p>
            </div>
          </div>
        </div>
      </section>
    </section>