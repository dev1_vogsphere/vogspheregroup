/* ============================================================
 * Plugin Core Init
 * For DEMO purposes only. Extract what you need.
 * ============================================================ */
$(document).ready(function() {
	'use strict';
    $('#contact-form').validate({
        // Override to submit the form via ajax
        submitHandler: function(form) {
            $('#contact-panel').portlet({
				refresh:true
			});
            $.ajax({
			     type:$(form).attr('method'),
			     url: $(form).attr('action'),
			     data: $(form).serialize(),
			     dataType: 'json',
			     success: function(data){
			     	console.log(data);
			       //Set your Succss Message
			       clearForm("Thank you! We will be in touch soon.");
			     },
			     error: function(err){
		            $('#contact-panel').portlet({
						refresh:false,
						//Set your ERROR Message
						error:"Uh-oh the sun fried the network, please try again."
					});
			     }
		    });
		    return false; // required to block normal submit since you used ajax
        }
    });
    function clearForm(msg){
    	$('#contact-panel').html('<div class="alert alert-success" role="alert">'+msg+'</div>');
    }
    
    $('#contact-panel').portlet({
        onRefresh: function() {
        }
    });
});