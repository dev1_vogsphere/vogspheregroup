<?php 
$content = '';
$ebook_url = '';
$ebook_picture = '';
$ebook_name = '';
$ebook_description = '';
$display = "style=display:none";

if(isset($_GET['name']))
{
	$ebook_name = $_GET['name'];
	$ebook_name = htmlspecialchars(urldecode(base64_decode($ebook_name)));
	$content = "Register to receive the complimentary copy of eBook : ".$ebook_name;
	$ebook_picture = $_GET['picture'];
	$ebook_url     = $_GET['url'];
	$ebook_description    = $_GET['description'];	
	/*
	$ebook_picture   	  = urldecode(base64_decode($ebook_picture));
	$ebook_url    		  = urldecode(base64_decode($ebook_url));
	$ebook_description    = htmlspecialchars(urldecode(base64_decode($ebook_description)));
	*/
	$display = "style=display:none";
}

?>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111474258-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111474258-1');
</script>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Contact Us (Get In Touch) | Vogsphere</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN PLUGINS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/swiper/css/swiper.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGINS -->
    <!-- BEGIN PAGES CSS -->
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="pages/css/pages-icons.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGES CSS -->
    <style>
	.status {
    /* display: none; */
    padding: 8px 35px 8px 14px;
    margin: 20px 0;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    color: #468847;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
		}
    </style>
  </head>
  <body class="pace-primary">
    <!-- BEGIN HEADER -->
    <?php include "menu.php";?>
    <!-- END HEADER -->
    <section class="m-t-60">
      <!-- START CONTACT SECTION -->
      <section class="container container-fixed-lg p-t-50 p-b-100 sm-p-b-30 sm-m-b-30">
        <div class="row">
          <div class="col-md-6">
            <!-- <div class="visible-xs visible-sm b-b b-grey-light m-t-35 m-b-30"></div>-->
            <div class="p-r-40 sm-p-l-0 sm-p-t-10">
              <div class="panel" id="contact-panel">
			  	<div  id="divContent">
                <p class="semi-bold no-margin">Get In Touch</p>
                <form role="form" autocomplete="off" class="m-t-15">
                        <input <?php echo $display; ?> type="text" id="ebook_picture" name="ebook_picture" class="form-control" 
						value="<?php echo $ebook_picture;?>" />
                        <input <?php echo $display; ?> type="text" id="ebook_url" name="ebook_url" class="form-control" 
						value="<?php echo $ebook_url;?>" />
                        <input <?php echo $display; ?> type="text" id="ebook_name" name="ebook_name" class="form-control" 
						value="<?php echo $ebook_name;?>" />
						<textarea  <?php echo $display; ?>  id="ebook_description"  name="ebook_description"><?php echo $ebook_description;?></textarea>						
						
                  <div class="row">
                    <div class="col-sm-6">
                      <!-- <div class="form-group form-group-default required">-->
					  <div id="FirstNameError" class="form-group form-group-default required">
                        <label class="control-label">Your Name</label>
                        <input type="text" id="name" name="name" class="form-control" required>
						<div class="col-sm-12">
							<small id="FirstNameErrorText" class="help-block" style=""></small>
						</div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div id="phoneError" class="form-group form-group-default required">
                        <label class="control-label">Mobile Number</label>
                        <input oninput="javascript: this.value=this.value.slice(0,this.maxLength);"
    type = "number"
    maxlength = "16" name="number" id="number" class="form-control" required>
											<div class="col-sm-12">
											<small id="phoneErrorText" class="help-block" style=""></small>
										</div>

                      </div>
                    </div>
                  </div>
                  <div id="emailError" class="form-group form-group-default required">
                    <label class="control-label">Email</label>
                    <input type="email" id="email" name="email" placeholder="The email you use everyday" class="form-control" required>
						<div class="col-sm-12">
							<small id="emailErrorText" class="help-block" style=""></small>
						</div>
                  </div>
					  <div id="companyError" class="form-group form-group-default required">
                        <label class="control-label">Company</label>
                        <input type="text" id="company" name="company" class="form-control" required>
						<div class="col-sm-12">
							<small id="companyErrorText" class="help-block" style=""></small>
						</div>
                      </div>
					<div class="row">
				   <div class="col-sm-6">
					  <div id="jobtitleError" class="form-group form-group-default required">
                        <label class="control-label">Job Title</label>
                        <input type="text" id="jobtitle" name="jobtitle" class="form-control" required>
						<div class="col-sm-12">
							<small id="jobtitleErrorText" class="help-block" style=""></small>
						</div>
                      </div>
                   </div>
				   <div class="col-sm-6">
					  <div id="departmentError" class="form-group form-group-default required">
                        <label class="control-label">Department</label>
                        <input type="text" id="department" name="department" class="form-control" required>
						<div class="col-sm-12">
							<small id="departmentErrorText" class="help-block" style=""></small>
						</div>
                      </div>
                  </div>
				  </div>				  
                  <div id="msgError" class="form-group form-group-default required">
                    <label class="control-label">Message</label>
                    <textarea id="message"  name="message" placeholder="Type the message you wish to send" style="height:100px" class="form-control" required><?php echo $content; ?></textarea>
                    <div class="col-sm-12">
					  <small id="msgErrorText" class="help-block" style=""></small>
					</div>
				  </div>
                  <div class="sm-p-t-10 clearfix">
                    <p class="pull-left small hint-text m-t-5 font-arial">The info I provided above is accurate. </p>
                    <!-- <button  onclick="validate_save()">Send Message</button> -->
					
					<button type="button" class="btn btn-primary font-montserrat all-caps fs-12 pull-right xs-pull-left" onclick="validate_save()">Send Message</button>
	
                  </div>
                  <div class="clearfix"></div>
                </form></div>
              </div>
            </div>
          </div>
		  
		            <div class="col-md-6">
            <h2 class="p-l-40 sm-p-r-0">
                How can we make you smile today?
            </h2>
            <div class="p-l-40 sm-p-r-0">
              <br>
              <div class="row">
                <div class="col-sm-6">
                  <h5 class="block-title hint-text m-b-0">
                            Johannesburg
                        </h5>
                  <address class="m-t-10">
                            Clearwater Office Park<br>
                            Strubens Valley<br>
                            Johannesburg
							1735
                        </address>
                  <br>
                  <p class="hint-text no-margin">
                    <span class="fs-12 font-montserrat bold all-caps p-r-10">Tel</span> <span class="fs-14">(+2710) 007-5969</span>
                  </p>
                <!--  <p class="hint-text">
                    <span class="fs-12 font-montserrat bold all-caps p-r-10">Fax</span> <span class="fs-14">(877) 412-7766</span>
                  </p>-->
                </div>
                <div class="col-sm-6">
                  <h5 class="block-title hint-text m-b-0">
                            Centurion
                        </h5>
                  <address class="m-t-10">
                            Mulder Street<br>
                            The Reeds EXT 26<br>
                            Centurion
							0158
                        </address>
                  <br>
                  <p class="hint-text no-margin">
                    <span class="fs-12 font-montserrat bold all-caps p-r-10">Tel</span> <span class="fs-14">(+2710) 007-5969</span>
                  </p>
                 <p class="hint-text">
                    <span class="fs-12 font-montserrat bold all-caps p-r-10">Email Address</span> <span class="fs-14">info@vogsphere.co.za</span>
                  </p>
                </div>
              </div>
              <br>
            </div>
          </div>
        </div>
      </section>
      <!-- END CONTACT SECION -->
    </section>
	 <!-- START FOOTER -->
	<?php include "footer.php"; ?>
    <!-- END FOOTER -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery-1.11.1.min.js"></script>
	<script>
	 // -- Enter to capture comments   ---
	 function validate_save()
	 {
		 msg  		  = document.getElementById('message').value;
	     FirstName	  = document.getElementById('name').value;
	     phone		  = document.getElementById('number').value;
	     email		  = document.getElementById('email').value;
		 
		 department   = document.getElementById('department').value;
		 jobtitle	  = document.getElementById('jobtitle').value;
		 company	  = document.getElementById('company').value;	
		 
		 ebook_url =  document.getElementById('ebook_url').value;	
		 ebook_picture =  document.getElementById('ebook_picture').value;	
		 ebook_name =  document.getElementById('ebook_name').value;	
		 ebook_description = document.getElementById('ebook_description').value;			 
		 //service = document.getElementById('service').value;
		 valid = true;

		if(msg != '' && FirstName != '' && email != '' && phone != '' && 
		   company != '' && jobtitle != '' && department != '')
		{
		  // -- phone
			if(phonenumber(phone) != true)
			{
				document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required has-error"); valid = false;
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter valid phone number';
		    }
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

		 // -- email
			if(ValidateEmail(email) != true)
			{
				document.getElementById("emailError").setAttribute("class", "form-group form-group-default required has-error"); valid = false;
				document.getElementById('emailErrorText').innerHTML = 'invalid email';
			}
			else
			{document.getElementById("emailError").setAttribute("class", "form-group form-group-default required");
			 document.getElementById('emailErrorText').innerHTML = '';
			}


		   // -- Message
			if(msg != '')
			{
				document.getElementById("msgError").setAttribute("class", "form-group form-group-default required");
				document.getElementById('msgErrorText').innerHTML = '';
		    }

			// -- First name
			if(FirstName != '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}
			
			// -- company
			if(company != '')
			{
				document.getElementById("companyError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('companyErrorText').innerHTML = '';
			}
			
			// -- Job
			if(jobtitle != '')
			{
				document.getElementById("jobtitleError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('jobtitleErrorText').innerHTML = '';
			}
			
			// -- Department
			if(department != '')
			{
				document.getElementById("departmentError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('departmentErrorText').innerHTML = '';
			}			
			
				if(valid)
				{		
							 //alert(msg+FirstName+email+phone);
						jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"jquery_contactus.php",
									   data:{msg:msg,FirstName:FirstName,phone:phone,email:email,
									    department:department,jobtitle:jobtitle,company:company,
										ebook_picture:ebook_picture,ebook_url:ebook_url,ebook_name:ebook_name,
										ebook_description:ebook_description},
										success: function(data)
										 {
											// alert(data);
											jQuery("#divContent").html(data);
										 }
									});
								});
					//document.getElementById('divContent').innerHTML = 'Message successfully submited.';
				}
	       }
		   else
		   {
			   // -- phone
			    if(phonenumber(phone) != true)
				{
					document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required has-error"); valid = false;
					document.getElementById('phoneErrorText').innerHTML = 'Please enter mobile number';
				}
				else
				{
					document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('phoneErrorText').innerHTML = '';
				}
				
				// -- Message
				if(msg == '')
				{
					document.getElementById("msgError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('msgErrorText').innerHTML = 'Please enter Message';
				}
				else
				{
					document.getElementById("msgError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('msgErrorText').innerHTML = '';
				}

				// -- First name
				if(FirstName == '')
				{
					document.getElementById("FirstNameError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('FirstNameErrorText').innerHTML = 'Please enter name';
				}
				else
				{
					document.getElementById("FirstNameError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('FirstNameErrorText').innerHTML = '';
				}
				
				// -- company
				if(company == '')
				{
					document.getElementById("companyError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('companyErrorText').innerHTML = 'Please enter company';
				}
				else
				{
					document.getElementById("companyError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('companyErrorText').innerHTML = '';
				}
				
				// -- jobtitle
				if(jobtitle == '')
				{
					document.getElementById("jobtitleError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('jobtitleErrorText').innerHTML = 'Please enter Job Title';
				}
				else
				{
					document.getElementById("jobtitleError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('jobtitleErrorText').innerHTML = '';
				}
				
				// -- department
				if(department == '')
				{
					document.getElementById("departmentError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('departmentErrorText').innerHTML = 'Please enter Department';
				}
				else
				{
					document.getElementById("departmentError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('departmentErrorText').innerHTML = '';
				}
				
				// -- email
				if(email == '')
				{	document.getElementById("emailError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('emailErrorText').innerHTML = 'Please enter email';
				}
				else
				{
					document.getElementById("emailError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('emailErrorText').innerHTML = '';
				}
		   }
		   return false;
		}
		 			
	
function phonenumber(inputtxt)
{
   if(isNaN(inputtxt))
   {
	    return false;
   }
   else if(inputtxt.length < 10)
   {
	    return false;
   }
   else
   {
        return true;
   }
}

function ValidateEmail(mail)
{

var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
var b=emailfilter.test(mail);

   if (b == true)
  {
    return true;
  }
   else
   {return false;}

}

	</script>
  </body>
</html>