<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111474258-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111474258-1');
</script>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Vogsphere - Creative Thinking Is Enforce</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web- app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN PLUGINS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/swiper/css/swiper.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGINS -->
    <!-- BEGIN PAGES CSS -->
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="pages/css/pages-icons.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN PAGES CSS -->
    <style>

    </style>
  </head>
  <body class="pace-primary">
    <!-- BEGIN HEADER -->
	<?php include "menu.php";?>
    <!-- END HEADER -->
    <!-- BEGIN JUMBOTRON -->
    <section class="jumbotron full-vh" data-pages="parallax">
      <div class="inner full-height">
        <!-- BEGIN SLIDER -->
        <div class="swiper-container" id="hero">
          <div class="swiper-wrapper">
            <!-- BEGIN SLIDE -->
            <div class="swiper-slide fit">
              <!-- BEGIN IMAGE PARRALAX -->
              <div class="slider-wrapper darken-overlay">
                <div class="background-wrapper" data-swiper-parallax="20%">
                  <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                  <div data-pages-bg-image="assets/images/WEBDEVELOPMENT.jpg" draggable="false" class="background"></div>
                </div>
              </div>
              <!-- END IMAGE PARRALAX -->
              <!-- BEGIN CONTENT -->
              <div class="content-layer">
                <div class="circular_object bg-primary" data-pages-animation="custom" data-attr="scale" data-start="21" data-end="0" data-duration="500" data-delay="600" data-lg-attr="scale" data-lg-start="26" data-lg-end="0" data-lg-duration="500" data-lg-delay="600" data-vlg-attr="scale" data-vlg-start="35" data-vlg-end="0" data-vlg-duration="500" data-vlg-delay="600">
                </div>
                <div class="inner full-height">
                  <div class="container-xs-height full-height">
                    <div class="col-xs-height col-middle text-left">
                      <div class="container">
                        <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 p-l-40 sm-no-padding" data-pages-animation="standard" data-type="transition.slideDownIn" data-duration="600" data-delay="1000" data-md-type="">
                          <h1 class="bold text-white sm-text-center">
                                                    Creative Thinking Is Enforced
                            </h1>
                          <p class="fs-20 text-white hint-text sm-text-center">We provide professional services, managed services, research and development and IT consulting for the modern business owner.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CONTENT -->
            </div>
            <!-- END SLIDE -->

            <!-- BEGIN SLIDE -->
            <div class="swiper-slide fit">
              <!-- BEGIN IMAGE PARRALAX -->
              <div class="slider-wrapper">
                <div class="background-wrapper" data-swiper-parallax="20%">
                  <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                  <div data-pages-bg-image="assets/images/ecashmeup.jpg" draggable="false" class="background"></div>
                </div>
              </div>
              <!-- END IMAGE PARRALAX -->
              <!-- BEGIN CONTENT -->
              <div class="content-layer">
                <div class="circular_object bg-info-darker" data-pages-animation="custom" data-attr="scale" data-start="21" data-end="0" data-duration="500" data-delay="600" data-lg-attr="scale" data-lg-start="26" data-lg-end="0" data-lg-duration="500" data-lg-delay="600" data-vlg-attr="scale" data-vlg-start="35" data-vlg-end="0" data-vlg-duration="500" data-vlg-delay="600">
                </div>
                <div class="inner full-height">
                  <div class="container-xs-height full-height">
                    <div class="col-xs-height col-middle text-left">
                      <div class="container">
                        <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 sm-no-padding p-l-40" data-pages-animation="standard" data-type="transition.slideDownIn" data-duration="600" data-delay="1000" data-md-type="">
                          <h1 class="bold text-white sm-text-center">
                                                   Simplifying Financial Services
                                                    </h1>
                          <p class="fs-20 text-white hint-text sm-text-center">An African team engineering financial solutions.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CONTENT -->
            </div>
            <!-- END SLIDE -->
						            <!-- BEGIN SLIDE -->
            <div class="swiper-slide fit">
              <!-- BEGIN IMAGE PARRALAX -->
              <div class="slider-wrapper">
                <div class="background-wrapper" data-swiper-parallax="20%">
                  <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                  <div data-pages-bg-image="assets/images/energyBanner.png" draggable="false" class="background"></div>
                </div>
              </div>
              <!-- END IMAGE PARRALAX -->
              <!-- BEGIN CONTENT -->
              <div class="content-layer">
                <div class="circular_object bg-warning-darker" data-pages-animation="custom" data-attr="scale" data-start="21" data-end="0" data-duration="500" data-delay="600" data-lg-attr="scale" data-lg-start="26" data-lg-end="0" data-lg-duration="500" data-lg-delay="600" data-vlg-attr="scale" data-vlg-start="35" data-vlg-end="0" data-vlg-duration="500" data-vlg-delay="600">
                </div>
                <div class="inner full-height">
                  <div class="container-xs-height full-height">
                    <div class="col-xs-height col-middle text-left">
                      <div class="container">
                        <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 p-l-40 sm-no-padding" data-pages-animation="standard" data-type="transition.slideDownIn" data-duration="600" data-delay="1000" data-md-type="">
                          <h1 class="bold text-white sm-text-center">
                                                    Sustainable Energy
                                                    </h1>
                          <p class="fs-20 text-white hint-text sm-text-center">Engineering sustainable energy solutions for Africa.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CONTENT -->
            </div>
            <!-- END SLIDE -->
						            <!-- BEGIN SLIDE -->
            <div class="swiper-slide fit">
              <!-- BEGIN IMAGE PARRALAX -->
              <div class="slider-wrapper">
                <div class="background-wrapper" data-swiper-parallax="20%">
                  <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                  <div data-pages-bg-image="assets/images/chicksbanner.jpg" draggable="false" class="background"></div>
                </div>
              </div>
              <!-- END IMAGE PARRALAX -->
              <!-- BEGIN CONTENT -->
              <div class="content-layer">
                <div class="circular_object bg-primary-dark" data-pages-animation="custom" data-attr="scale" data-start="21" data-end="0" data-duration="500" data-delay="600" data-lg-attr="scale" data-lg-start="26" data-lg-end="0" data-lg-duration="500" data-lg-delay="600" data-vlg-attr="scale" data-vlg-start="35" data-vlg-end="0" data-vlg-duration="500" data-vlg-delay="600">
                </div>
                <div class="inner full-height">
                  <div class="container-xs-height full-height">
                    <div class="col-xs-height col-middle text-left">
                      <div class="container">
                        <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 p-l-40 sm-no-padding" data-pages-animation="standard" data-type="transition.slideDownIn" data-duration="600" data-delay="1000" data-md-type="">
                          <h1 class="bold text-white sm-text-center">
                                                    Sustainable Farming
                                                    </h1>
                          <p class="fs-20 text-white hint-text sm-text-center">Engineering sustainable farming solutions for Africa.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CONTENT -->
            </div>
            <!-- END SLIDE -->
									            <!-- BEGIN SLIDE -->
            <div class="swiper-slide fit">
              <!-- BEGIN IMAGE PARRALAX -->
              <div class="slider-wrapper">
                <div class="background-wrapper" data-swiper-parallax="20%">
                  <!-- YOUR BACKGROUND IMAGE HERE, YOU CAN ALSO USE IMG with the same classes -->
                  <div data-pages-bg-image="assets/images/waterBanner.jpg" draggable="false" class="background"></div>
                </div>
              </div>
              <!-- END IMAGE PARRALAX -->
              <!-- BEGIN CONTENT -->
              <div class="content-layer">
                <div class="circular_object bg-danger-darker" data-pages-animation="custom" data-attr="scale" data-start="21" data-end="0" data-duration="500" data-delay="600" data-lg-attr="scale" data-lg-start="26" data-lg-end="0" data-lg-duration="500" data-lg-delay="600" data-vlg-attr="scale" data-vlg-start="35" data-vlg-end="0" data-vlg-duration="500" data-vlg-delay="600">
                </div>
                <div class="inner full-height">
                  <div class="container-xs-height full-height">
                    <div class="col-xs-height col-middle text-left">
                      <div class="container">
                        <div class="col-md-6 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 p-l-40 sm-no-padding" data-pages-animation="standard" data-type="transition.slideDownIn" data-duration="600" data-delay="1000" data-md-type="">
                          <h1 class="bold text-white sm-text-center">
                                                    Sustainable Water
                                                    </h1>
                          <p class="fs-20 text-white hint-text sm-text-center">Engineering sustainable water purifying solutions for Africa.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END CONTENT -->
            </div>
            <!-- END SLIDE -->
          </div>
          <!-- Add Navigation -->
          <!-- Add Navigation -->
          <div class="swiper-navigation swiper-rounded swiper-white-solid swiper-button-prev"></div>
          <div class="swiper-navigation swiper-rounded swiper-white-solid swiper-button-next"></div>
        </div>
      </div>
      <!-- END SLIDER -->
      
    </section>
    <!-- END JUMBOTRON -->
    <!-- BEGIN CONTENT BAR -->
  


<section class=" jumbotron bg-master-darker text-center text-white">
</br>
</br>

</br>
</br>
<div class="row">
<div class="col-sm-4">
<h2 class="bold text-white sm-text-center">Our Core Values</h2>
  <div class="embed-responsive embed-responsive-16by9">
    <!--<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/I7e-Kl40LD4" allowfullscreen></iframe>-->
	<iframe width="560" height="315" src="https://www.youtube.com/embed/ZAeTwVYfNog" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
  </div>
</div>
<div class="col-sm-4">
<h2 class="bold text-white sm-text-center">Insurance Broker CRM</h2>
  <div class="embed-responsive embed-responsive-16by9">
    <!--<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vZ7YbUx21kg" allowfullscreen></iframe>-->
    <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/6oESv7R0e1g?start=2" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div>
</div>
<div class="col-sm-4">
<h2 class="bold text-white sm-text-center">Shares Investing</h2>
<div class="embed-responsive embed-responsive-16by9">
   <!-- <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/vZ7YbUx21kg" allowfullscreen></iframe>-->
   <!-- <iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/u9T2vb1UEk4?start=13" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>-->
  <iframe width="560" height="315" src="https://www.youtube.com/embed/pBWPTFwY_Xo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
  </div>
</div>
</div>
<div>

</section>
<section class=" jumbotron bg-master-darker text-center text-white">
</br>
</br>
<h2 class="inline text-white xs-m-b-20">Join us as we transform Africa.</h2>
      <br>
      <br>
      <div><a class="btn btn-cons btn-bordered m-l-20 xs-no-margin" type="button" href="contact">Get In Touch</a></div>
<br> 
<br>
<br>
<br>

</section>

  
	 <!-- START FOOTER -->
	<?php include "footer.php"; ?>
    <!-- END FOOTER -->

    <!-- BEGIN CORE FRAMEWORK -->
    <script src="assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="pages/js/pages.image.loader.js"></script>
    <script type="text/javascript" src="assets/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- BEGIN SWIPER DEPENDENCIES -->
    <script type="text/javascript" src="assets/plugins/swiper/js/swiper.jquery.min.js"></script>
    <script type="text/javascript" src="assets/plugins/velocity/velocity.min.js"></script>
    <script type="text/javascript" src="assets/plugins/velocity/velocity.ui.js"></script>
    <!-- BEGIN RETINA IMAGE LOADER -->
    <script type="text/javascript" src="assets/plugins/jquery-unveil/jquery.unveil.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN PAGES FRONTEND LIB -->
    <script type="text/javascript" src="pages/js/pages.frontend.js"></script>
    <!-- END PAGES LIB -->
    <!-- BEGIN YOUR CUSTOM JS -->
    <script type="text/javascript" src="assets/js/custom.js"></script>
    <!-- END PAGES LIB -->
  </body>
</html>
