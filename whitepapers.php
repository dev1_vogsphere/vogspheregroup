<?php 
require 'database.php'; 
// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// ------ Customer Data to Validate Against -- //
$sql = 'SELECT * FROM whitepapers';
$dataWhitepapers = $pdo->query($sql);	
?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-111474258-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-111474258-1');
</script>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <title>Contact Us (Get In Touch) | Vogsphere</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="apple-touch-icon" href="pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN PLUGINS -->
    <link href="assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/swiper/css/swiper.css" rel="stylesheet" type="text/css" media="screen" />
    <!-- END PLUGINS -->
    <!-- BEGIN PAGES CSS -->
    <link class="main-stylesheet" href="pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="pages/css/pages-icons.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="hs_default_custom_style.min.css" rel="stylesheet" type="text/css" />
	
    <!-- BEGIN PAGES CSS -->
    <style>
	.status {
    /* display: none; */
    padding: 8px 35px 8px 14px;
    margin: 20px 0;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
    color: #468847;
    background-color: #dff0d8;
    border-color: #d6e9c6;
    -webkit-border-radius: 4px;
    -moz-border-radius: 4px;
    border-radius: 4px;
		}
		
.Webinars {
    max-width: 100%;/*964px;*/
    margin: 0 auto;
    padding: 44px 16px 24px;
	display: inline-block;
}

.Webinars-itemTitle {
    margin: 0;
    padding: 0;
    font-size: 18px;
    font-weight: 600;
    color: #1b1b1e;
	line-height: 1.2;
}

.Webinars-items {
    list-style: none;
    padding: 0;
    margin: 0;
    font-size: 0;
}

.Webinars-item:not(:last-child) {
    margin: 0 0 28px;
}
.Webinars-item {
    text-align: left;
}	

.Webinars-itemDate {
    color: #f14d0d;
    font-weight: 700;
}

.Webinars-itemStats {
    margin: 0 0 2px;
    font-size: 16px;
    font-weight: 600;
    color: #1b1b1e;
}
.Webinars-itemDesc {
    margin: 0;
    font-size: 14px;
}
.Webinars-itemPic {
    position: relative;
    width: 100%;
    margin: 0 0 6px;
    padding-bottom: 56.25%;
    height: 0;
    overflow: hidden;
    border-radius: 3px;
}
p, li {
    font-size: 18px;
    color: #131d40;
}

figure{
    display: block;
}

.Webinars-itemCategory:before {
    content: "|";
    display: inline;
    margin: 0 2px;
    color: #f14d0d;
}
    </style>
  </head>
  <body class="pace-primary">
    <!-- BEGIN HEADER -->
   	<?php include "menu.php";?>
    <!-- END HEADER -->
    <section class="m-t-60">
      <!-- START CONTACT SECTION -->
      <div class="header320">
		<div class="container">
			<div class="row-fluid">
				 <div class="header320-text text-center">
					<h1>White papers</h1>
					<h6>Get our in-depth research</h6>
				 </div>
			</div>
		</div>
	 </div>
      <!-- END CONTACT SECION -->
	<section class="Webinars w-upcoming">
		<ul class="Webinars-items" style="text-align: left;">
		 <?php 
			foreach($dataWhitepapers as $row)
			{
				//$FrequencyArr[] = $row['statusId'];
			$documentEncoded = base64_encode(urlencode($row['document'])); 
			$nameEncoded = base64_encode(urlencode($row['name']));
			$picture = base64_encode(urlencode($row['picture']));
			$description = base64_encode(urlencode($row['description']));
			
		 ?>
		 <li class="Webinars-item itemsBox moreBox col-sm-4" style="display: inline-block;">
             <a href="contact?name=<?php echo $nameEncoded;?>&url=<?php echo $documentEncoded;?>&picture=<?php echo $picture;?>&
					  description=<?php echo $description; ?>">
              <figure class="Webinars-itemPic">
                  <img src="<?php echo $row['picture']; ?>" alt="robots_rescue">
              </figure>
              <h4 class="Webinars-itemTitle"><?php echo $row['name']; ?></h4>
              <div class="Webinars-itemStats">
                <time class="Webinars-itemDate"><?php echo $row['date']; ?></time>
                <span class="Webinars-itemCategory"><?php echo $row['category']; ?></span>
              </div>
              <p class="Webinars-itemDesc"></p><p style="font-size: 14px;">
				  <span>
					<?php echo  $row['description']; ?>
				  </span>
			  </p><p></p>
            </a>
          </li>
		  <?php }?>
		</ul>
	</section>
    </section>
	 <!-- START FOOTER -->
<?php include "footer.php";?>
    <!-- END FOOTER -->
    <script type="text/javascript" src="assets/plugins/jquery/jquery-1.11.1.min.js"></script>
	<script>
	 // -- Enter to capture comments   ---
	 function validate_save()
	 {
		 msg  		  = document.getElementById('message').value;
	     FirstName	  = document.getElementById('name').value;
	     phone		  = document.getElementById('number').value;
	     email		  = document.getElementById('email').value;
		 
		 //service = document.getElementById('service').value;
		 valid = true;

		if(msg != '' && FirstName != '' && email != '' && phone != '')
		{
		  // -- phone
			if(phonenumber(phone) != true)
			{
				document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required has-error"); valid = false;
			    document.getElementById('phoneErrorText').innerHTML = 'Please enter valid phone number';
		    }
			else
			{
				document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('phoneErrorText').innerHTML = '';
			}

		 // -- email
			if(ValidateEmail(email) != true)
			{
				document.getElementById("emailError").setAttribute("class", "form-group form-group-default required has-error"); valid = false;
				document.getElementById('emailErrorText').innerHTML = 'invalid email';
			}
			else
			{document.getElementById("emailError").setAttribute("class", "form-group form-group-default required");
			 document.getElementById('emailErrorText').innerHTML = '';
			}


		   // -- Message
			if(msg != '')
			{
				document.getElementById("msgError").setAttribute("class", "form-group form-group-default required");
				document.getElementById('msgErrorText').innerHTML = '';
		    }

		    // -- Company name
			if(FirstName != '')
			{
				document.getElementById("FirstNameError").setAttribute("class", "form-group form-group-default required");
			    document.getElementById('FirstNameErrorText').innerHTML = '';
			}
				if(valid)
				{		
							 //alert(msg+FirstName+email+phone);
						jQuery(document).ready(function(){
						jQuery.ajax({  type: "POST",
									   url:"jquery_contactus.php",
									   data:{msg:msg,FirstName:FirstName,phone:phone,email:email},
										success: function(data)
										 {
											// alert(data);
											jQuery("#divContent").html(data);
										 }
									});
								});
					//document.getElementById('divContent').innerHTML = 'Message successfully submited.';
				}
	       }
		   else
		   {
			   // -- phone
			    if(phonenumber(phone) != true)
				{
					document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required has-error"); valid = false;
					document.getElementById('phoneErrorText').innerHTML = 'Please enter mobile number';
				}
				else
				{
					document.getElementById("phoneError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('phoneErrorText').innerHTML = '';
				}
				
				// -- Message
				if(msg == '')
				{
					document.getElementById("msgError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('msgErrorText').innerHTML = 'Please enter Message';
				}
				else
				{
					document.getElementById("msgError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('msgErrorText').innerHTML = '';
				}

				// -- Company name
				if(FirstName == '')
				{
					document.getElementById("FirstNameError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('FirstNameErrorText').innerHTML = 'Please enter name';
				}
				else
				{
					document.getElementById("FirstNameError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('FirstNameErrorText').innerHTML = '';
				}
				
				// -- email
				if(email == '')
				{	document.getElementById("emailError").setAttribute("class", "form-group form-group-default required has-error");
					document.getElementById('emailErrorText').innerHTML = 'Please enter email';
				}
				else
				{
					document.getElementById("emailError").setAttribute("class", "form-group form-group-default required");
					document.getElementById('emailErrorText').innerHTML = '';
				}
		   }
		   return false;
		}
		 			
	
function phonenumber(inputtxt)
{
   if(isNaN(inputtxt))
   {
	    return false;
   }
   else if(inputtxt.length < 10)
   {
	    return false;
   }
   else
   {
        return true;
   }
}

function ValidateEmail(mail)
{

var emailfilter=/^\w+[\+\.\w-]*@([\w-]+\.)*\w+[\w-]*\.([a-z]{2,4}|\d+)$/i
var b=emailfilter.test(mail);

   if (b == true)
  {
    return true;
  }
   else
   {return false;}

}

	</script>
  </body>
</html>