<?php
require 'database.php';

// -- Database Declarations and config:  
$pdo = Database::connect();
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// -- Declarations -- //
$today  = date('Y-m-d');
$userid = '9901019999999';
$status = 'open';
$count  = 0;
$customeridError = '';
$valid 			 = true;
$agentid 		 = '9901019999999';
$LastName 		 = '';
$tbl_lead 		 = 'lead';
$email 		= getpost('email');

// ------ Customer Data to Validate Against -- //
$sql = 'SELECT * FROM customer';
$dataCustomers = $pdo->query($sql);		
// ------ Company Details	-- //
$Title 		= 'comp';
$FirstName  = getpost('FirstName');
// ------ Contact Details  -- //
$phone 		= getpost('phone');
$customerid = '';
//$service    = $_POST['service'];
$comments     = "Send Message via - website on ".$today;

$department   = getpost('department');
$jobtitle	  = getpost('jobtitle');
$company	  = getpost('company');	

$msg        = 'From Name : '.$FirstName."<br/>".
			  "Company :".$company."<br/>".
			  "Job Title :".$jobtitle."<br/>".
			  "Department :".$department."<br/>".			  
			  "Email : ".$email."<br/>".getpost('msg');

// -- White Paper Details.
$ebook_picture = getpost('ebook_picture');
$ebook_url     = getpost('ebook_url');
$ebook_name    		  = getpost('ebook_name');
$ebook_description    = getpost('ebook_description');
// -- Decode Back
if(!empty($ebook_name))
{
	$ebook_picture   	  = urldecode(base64_decode($ebook_picture));
	$ebook_url    		  = urldecode(base64_decode($ebook_url));
	$ebook_description    = htmlspecialchars(urldecode(base64_decode($ebook_description)));
}		  
// check if maintained globally first.
$emailAddress = '';
if(getsession('Globalemail'))
{$emailAddress = getsession('Globalemail');}
else{$emailAddress = 'info@vogsphere.co.za';}

$subject = 'Enquiry from website on '.$today;
// -- Validate
//if (!empty($phone)) { if(!CheckCommunication($phone,$email)){$valid = false;	}}		
//if (!empty($email)) { if(!CheckCommunication($phone,$email)){$valid = false;	}}	

		if ($valid) 
		{
			// --- Customer Details
			$values = array($Title,$FirstName,$LastName,$status,$phone,$email,$comments,$userid,$today,$agentid,$today,$customerid,$department,$jobtitle,$company);
			//print_r($values);
			$sql = "INSERT INTO vogspesw_siwapp.lead (title,name,surname,status,phone,email,comments,createdby,createdon,agentid,assignedon,customerid,department,jobtitle,company)";
			$today = date('Y-m-d');
			
			$sql = $sql." VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			//echo $sql;
			$q = $pdo->prepare($sql);
			$q->execute($values);      
		
			$message = $Title.' '.$FirstName.' '.$LastName;
			$count = 1; 
			//Database::disconnect();
						
			$html = "<p class='status'> Message successfully submited. We will be in touch.</p>";	
		
		// -- Send auto Admin Email.						
			SendEmail($msg,$emailAddress,$subject);
		// -- Send an auto reply to the client.
			if(!empty($ebook_url))
			{
				$html = file_get_contents('email_whitepaper.php');
				$html = str_replace("[TITLE]",$ebook_name,$html); 
				$html = str_replace("[PICTURE]",$ebook_picture,$html); 
				$html = str_replace("[DESCRIPTION]",$ebook_description,$html); 
				$html = str_replace("[DOWNLOAD]",$ebook_url,$html); 
				$subject = "[FREE EBOOK]{$ebook_name}";
				SendEmail($html,$email,$subject);
			}
		}
		//else
		//{
			//$html = $customeridError;				
		//}
		$html = "<p class='status'> Message successfully submited. We will be in touch.</p>";	
        echo $html;		
?>