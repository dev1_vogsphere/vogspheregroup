<?php
$menuitem = '';
$menuclass = '';
$page = $_SERVER['REQUEST_URI'];
// -- Active Menu Links.
$contactActive 	   = '';
$whitepapersActive = '';
$indexActive 	   = '';
if(strpos($page, "contact") !== false)
{
	$menuclass = 'class="header bg-header minimized dark"';
	$contactActive = 'class="active"';
}
else if(strpos($page, "whitepapers") !== false)
{
	$menuclass = 'class="header bg-header minimized dark"';
	$whitepapersActive = 'class="active"';
}
else if(strpos($page, "index") !== false)
{
	$menuclass   = 'class="header bg-header transparent-light "';
	$indexActive = 'class="active"';
}
else
{
	$menuclass   = 'class="header bg-header transparent-light "';
	$indexActive = 'class="active"';

	    /* <nav class="header bg-header transparent-light " data-pages="header"
		data-pages-header="autoresize" data-pages-resize-class="dark"> */
}
?>
<!-- <nav class="header bg-header transparent-light " data-pages="header" data-pages-header="autoresize" data-pages-resize-class="dark"> -->
    <nav  <?php echo $menuclass; ?> data-pages="header" data-pages-header="autoresize" data-pages-resize-class="dark">
      <div class="container relative">
        <!-- BEGIN LEFT CONTENT -->
        <div class="pull-left">
          <!-- .header-inner
                Allows to horizontally Align elements to the Center
                 -->
          <div class="header-inner">
            <!-- BEGIN LOGO -->
            <a href="index"><img src="assets/images/weblogovogspherewhite.png" width="349" height="56" data-src-retina="assets/images/weblogovogspherewhite.png" class="" alt=""> </a>
          </div>
        </div>
        <div class="pull-right">
          <div class="header-inner">
            <a href="#" data-toggle="search" class="search-toggle visible-sm-inline visible-xs-inline p-r-10"><i class="fs-14 pg-search"></i></a>
            <div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10" data-pages="header-toggle" data-pages-element="#header">
              <div class="one"></div>
              <div class="two"></div>
              <div class="three"></div>
            </div>
          </div>
        </div>
        <!-- BEGIN RIGHT CONTENT -->
        <div class="menu-content mobile-dark pull-right clearfix" data-pages-direction="slideRight" id="header">
          <!-- BEGIN HEADER CLOSE TOGGLE FOR MOBILE -->
          <div class="pull-right">
            <a href="#" class="padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10" data-pages="header-toggle" data-pages-element="#header">
              <i class=" pg-close_line"></i>
            </a>
          </div>
          <!-- END HEADER CLOSE TOGGLE FOR MOBILE -->
          <!-- BEGIN MENU ITEMS -->
          <div class="header-inner">
            <ul class="menu">
              <li>
                <a <?php echo $indexActive; ?> data-text="Home" href="index">Home <span data-text=
                            "Welcome">Welcome</span></a>
              </li>
              <li class="classic multiline">
                <a href="javascript:;" data-text="Business">Business <i class="pg-arrow_minimize m-l-5"></i>
                    <span data-text="Portfolio">Portfolio</span></a>
                <nav class="classic ">
                  <span class="arrow"></span>
                  <ul>
                    <li>
                      <a href="https://www.ecashmeup.com/" target="_blank">eCashMeUp</a>
                    </li>
                    <li>
                     <a href="#">Energy (coming soon!)</a>
                    </li>
                    <li>
                      <a href="#">Farming (coming soon!)</a>
                    </li>
                    <li>
                      <a href="#">Water (coming soon!)</a>
                    </li>
                  </ul>
                </nav>
              </li>
			  <li>
                <a <?php echo $whitepapersActive; ?> href="whitepapers" data-text="White Papers">White Papers
                <span data-text="Get in touch">Get our in-depth research</span></a>
              </li>
			  <li>
                <a <?php echo $contactActive; ?> href="contact" data-text="Contact Us">Contact Us
                <span data-text="Get in touch">Get in touch </span></a>
              </li>
            <!-- BEGIN COPYRIGHT FOR MOBILE -->
            <div class="font-arial m-l-35 m-r-35 m-b-20  visible-sm visible-xs m-t-30">
              <p class="fs-11 small-text muted">Copyright &copy; <?php echo date('Y'); ?> Vogsphere</p>
            </div>
            <!-- END COPYRIGHT FOR MOBILE -->
          </div>
          <!-- END MENU ITEMS -->
        </div>
      </div>
    </nav>
